import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.NavigableSet;

public class kadai {
    private JButton cocacolaB;
    private JButton coffeeB;
    private JButton gogoteaB;
    private JButton meronsodaB;
    private JButton waterB;
    private JButton smiileB;
    private JLabel order;
    private JTextPane chumon;
    private JPanel root;
    private JButton checkoutB;
    private JLabel TOTAL;
    int cocaP=100,coffP=200,gogoP=300,meronP=400,waterP=500,smileP=0;
    double total=0;

    void order(String food ){
        int confirmation = JOptionPane.showConfirmDialog(null,
                "would you like to order "+food+"?",
                "order confirmation",
                JOptionPane.YES_NO_OPTION);
        if(confirmation==0){
            JOptionPane.showMessageDialog(null ,
                    "order for "+food+" received");

            String currentText = chumon.getText();
            chumon.setText(currentText+food+"\n");

            switch(food){
                case "cocacola":
                    total+=cocaP;
                    break;
                    case "coffee":
                    total+=coffP;
                    break;
                case"gogotea":
                        total+=gogoP;
                    break;
                case"meronsoda":
                    total+=meronP;
                    break;
                case"water":
                    total+=waterP;
                    break;
                case"smile":
                    total+=smileP;
                    break;

            }

            TOTAL.setText("total "+(int)total+"yen (excluding tax)");

        }
    }

    public kadai() {
        cocacolaB.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {order("cocacola");
            }

        });
        cocacolaB.setIcon(new ImageIcon( this.getClass().getResource("coca.png")
        ));
        cocacolaB.setText("cocacola "+cocaP+"yen");


        coffeeB.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {order("coffee");
            }
        });
        coffeeB.setIcon(new ImageIcon( this.getClass().getResource("coffee.jpg")
        ));
        coffeeB.setText("coffee "+coffP+"yen");

        gogoteaB.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {order("gogotea");
            }
        });
        gogoteaB.setIcon(new ImageIcon( this.getClass().getResource("gogotea.jpg")
        ));
        gogoteaB.setText("gogotea "+gogoP+"yen");

        meronsodaB.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {order("meronsoda");
            }
        });
        meronsodaB.setIcon(new ImageIcon( this.getClass().getResource("popmelon.jpg")
        ));
        meronsodaB.setText("melonsoda "+meronP+"yen");

        waterB.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {order("water");
            }
        });
        waterB.setIcon(new ImageIcon( this.getClass().getResource("S00656_2.jpg")
        ));
        waterB.setText("water "+waterP+"yen");

        smiileB.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {order("smile");

            }
        });
        smiileB.setIcon(new ImageIcon( this.getClass().getResource("smile.jpg")
        ));
        smiileB.setText("smile "+smileP+"yen");

        checkoutB.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int a = JOptionPane.showConfirmDialog(null,
                        "Would you like to Checkout?",
                        "Checkout Confilmation",
                        JOptionPane.YES_NO_OPTION);
                if(a==0){
                    int b = JOptionPane.showConfirmDialog(null,
                            "if you take it home,reduced tax rate applied!\nDo you take it home?",
                            "reduced tax rate ",
                            JOptionPane.YES_NO_OPTION);
                    if (b==0) {
                        total=total*1.08;
                    }else{total=total*1.1;
                    }

                    JOptionPane.showMessageDialog(null,"Thank you! Total price is "+(int)total+" yen.");
                    chumon.setText("");
                    total=0;
                    TOTAL.setText("total "+(int)total+"yen(including tax)");
                }
            }
        });
    }

    public static void main(String[] args) {
        JFrame frame = new JFrame("kadai");
        frame.setContentPane(new kadai().root);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
    }
}
